import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';

import NetworkComponent from "../NetworkComponent";
import Container from "@material-ui/core/Container";
import List from "@material-ui/core/List";
import {ListItemText} from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "@material-ui/core/FormGroup";
import register_plugin from "../pluginHelper";


const style = theme => ({
    root: {
        height: '100%',
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(1)
    },
    dangerbox: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(1),
        backgroundColor: "#ff9095"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        width: '100%'
    }
});

class UserManagement extends NetworkComponent {

    constructor(props) {
        super(props);

        this.state = {
            users: [],
            root: false,
            selectedUser: userData,
            selectedUserIndex: 0
        }
    }

    componentDidMount() {
        if (userData.isRoot)
            this.GET("/user/list").then(res => this.setState({users: res.data.data || []}));
        else
            this.setState({users: [userData]});
    }

    closeDialog() {
        this.setState({chpsw: false, editData: false, newUser: false, errormsg: ""});
        this.pswa = "";
        this.pswb = "";
    }

    changePassword() {
        if (this.pswa !== this.pswb) {
            this.setState({errormsg: "Password diverse."});
            return;
        }

        this.POST(`/user/${this.state.selectedUser.username}/chpsw`, {password: this.pswa}).then(res => {
            if (res.data.status) this.closeDialog();
            else this.setState({errormsg: res.data.msg});
        });
    }

    removeGroup(gr, i) {
        this.REQUEST("DELETE", `/user/${this.state.selectedUser.username}/group/${gr}`).then(res => {
            if (!res.data.status) alert("Errore.");
            else this.state.selectedUser.groups.splice(i, 1);
            this.forceUpdate()
        })
    }

    addGroup(gr) {
        this.REQUEST("PUT", `/user/${this.state.selectedUser.username}/group/${gr}`).then(res => {
            if (!res.data.status) alert("Errore.");
            else this.state.selectedUser.groups.push(gr);
            this.forceUpdate();
        })
    }

    editValue() {
        this.POST(`/user/${this.state.selectedUser.username}/data`, {[this.state.changingKey]: this.newValue}).then(res => {
            if (res.data.status) {
                this.state.selectedUser.data[this.state.changingKey] = this.newValue;
                this.newValue = "";
                this.closeDialog();
            } else alert(res.data.msg)
        });
    }

    userDel() {
        if (confirm("Sei sicuro?"))
            this.REQUEST("DELETE", `/user/${this.state.selectedUser.username}`).then(res => {
                if (res.data.status) {
                    this.state.users.splice(this.state.selectedUserIndex, 1);
                    this.setState({selectedUser: this.state.users[0]})
                } else {
                    alert(res.data.msg)
                }
            })
    }

    setFlag(state) {
        this.POST(`/user/${this.state.selectedUser.username}/status`, {enabled: state}).then(res => {
            if (res.data.status) {
                this.state.selectedUser.enabled = state;
                this.forceUpdate();
            } else alert(res.data.msg)
        });
    }

    newUser() {
        if (this.pswa !== this.pswb) {
            this.setState({errormsg: "Password diverse."});
            return;
        }

        this.POST(`/user/new`, {
            username: this.user,
            password: this.pswa,
            root: this.state.root,
            data: {}
        }).then(res => {
            console.log(res);
            if (res.data.status) {
                let data = JSON.parse(JSON.stringify(this.state.selectedUser.data));
                Object.keys(data).forEach(e => data[e] = null);
                this.state.users.push({username: this.user, groups: [], isRoot: this.state.root, data, enabled: true});
                this.closeDialog();
            } else this.setState({errormsg: res.data.msg});
        });
    }

    render() {
        return (
            <div className={this.props.classes.root}>
                <Container component="main" maxWidth="sm">
                    <CssBaseline/>
                    <Paper className={this.props.classes.paper}>
                        <div className={this.props.classes.paper}>
                            <Grid container>
                                <Grid item xs={5}>
                                    <Typography component="h1" variant="h5">
                                        Gestione Account
                                    </Typography>
                                </Grid>
                                <Grid item xs={5}>
                                    {!userData.isRoot ? <Typography component="h1"
                                                                    variant="h5">{this.state.selectedUser.username}</Typography> :
                                        <FormControl className={this.props.classes.formControl}>
                                            <InputLabel>Utente</InputLabel>
                                            <Select
                                                className={this.props.classes.submit}
                                                value={this.state.selectedUserIndex}
                                                onChange={(e) => this.setState({
                                                    selectedUser: this.state.users[e.target.value],
                                                    selectedUserIndex: e.target.value
                                                })}
                                            >
                                                {this.state.users.map((e, i) => <MenuItem key={i}
                                                                                          value={i}>{e.username}</MenuItem>)}
                                            </Select>
                                        </FormControl>
                                    }
                                </Grid>
                                {userData.isRoot && <Grid item xs={2}>
                                    <Button className={this.props.classes.submit} color={"primary"}
                                            variant={"contained"}
                                            onClick={() => this.setState({newUser: true})}><AddIcon/></Button>
                                </Grid>}
                                <Grid item xs={12}> <Button
                                    color={"primary"}
                                    className={this.props.classes.submit}
                                    onClick={() => this.setState({chpsw: true})}
                                    variant={"contained"}>Cambia Password</Button>
                                </Grid>
                                <Grid item xs={12}/>
                                <Grid item xs={12}>
                                    <Typography>Gruppi:</Typography>
                                    <List>
                                        {this.state.selectedUser.groups.map((e, i) =>
                                            <ListItem key={e}>
                                                <ListItemText primary={e}/>
                                                {userData.isRoot && <ListItemSecondaryAction>
                                                    <IconButton edge="end" onClick={() => this.removeGroup(e, i)}>
                                                        <DeleteIcon/>
                                                    </IconButton>
                                                </ListItemSecondaryAction>}
                                            </ListItem>)}
                                    </List>
                                </Grid>
                                {userData.isRoot && [
                                    <Grid item xs={9}>
                                        <TextField
                                            className={this.props.classes.submit}
                                            fullWidth
                                            placeholder="Nuovo gruppo"
                                            onChange={(e) => {
                                                this.grname = e.target.value
                                            }}
                                        />
                                    </Grid>,
                                    <Grid item xs={3}>
                                        <Button className={this.props.classes.submit} color={"secondary"}
                                                variant={"contained"}
                                                onClick={() => this.addGroup(this.grname)}><AddIcon/></Button>
                                    </Grid>
                                ]}
                                <Grid item xs={12}>
                                    <Typography>Chavi:</Typography>
                                    <List>
                                        {Object.keys(this.state.selectedUser.data).map(e =>
                                            <ListItem key={e}>
                                                <ListItemText primary={`${e}: ${this.state.selectedUser.data[e]}`}/>
                                                <ListItemSecondaryAction>
                                                    {(userData.isRoot || !e.startsWith("root_")) &&
                                                    <IconButton edge="end" onClick={() => this.setState({
                                                        changingKey: e,
                                                        editData: true
                                                    })}>
                                                        Modifica
                                                    </IconButton>}
                                                </ListItemSecondaryAction>
                                            </ListItem>
                                        )}
                                    </List>
                                </Grid>
                            </Grid>
                        </div>
                    </Paper>
                    {userData.isRoot && <Paper className={this.props.classes.dangerbox}>
                        <Typography variant={"h3"}>DangerZone!</Typography>
                        <Grid container>
                            <Grid item xs={12}>
                                <Button color={"primary"} className={this.props.classes.submit}
                                        onClick={() => this.userDel()}
                                        variant={"contained"}>Elimina Account</Button>
                            </Grid>
                            <Grid item xs={12}>
                                {this.state.selectedUser.enabled &&
                                <Button color={"primary"} className={this.props.classes.submit}
                                        onClick={() => this.setFlag(false)}
                                        variant={"contained"}>Disabilita</Button>}

                                {!this.state.selectedUser.enabled &&
                                <Button color={"primary"} className={this.props.classes.submit}
                                        onClick={() => this.setFlag(true)}
                                        variant={"contained"}>Riabilita</Button>}
                            </Grid>
                        </Grid>
                    </Paper>}
                </Container>

                <Dialog open={this.state.chpsw} onClose={() => this.closeDialog()}>
                    <DialogTitle>Cambio password per {this.state.selectedUser.username}</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Nuova password"
                            type="password"
                            onChange={(e) => this.pswa = e.target.value}
                            fullWidth
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Ripeti password"
                            type="password"
                            onChange={(e) => this.pswb = e.target.value}
                            fullWidth
                        />
                        <Typography variant={"h6"} color={"red"}>{this.state.errormsg}</Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.closeDialog()} color="primary">
                            Annulla
                        </Button>
                        <Button onClick={() => this.changePassword()} color="primary">
                            Cambia
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.editData} onClose={() => this.closeDialog()}>
                    <DialogTitle>Modifica della
                        chiave {this.state.changingKey} di {this.state.selectedUser.username}</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Nuovo valore"
                            onChange={(e) => this.newValue = e.target.value}
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.closeDialog()} color="primary">
                            Annulla
                        </Button>
                        <Button onClick={() => this.editValue()} color="primary">
                            Cambia
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.newUser} onClose={() => this.closeDialog()}>
                    <DialogTitle>Nuovo Utente</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Username"
                            onChange={(e) => this.user = e.target.value}
                            fullWidth
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Nuova password"
                            type="password"
                            onChange={(e) => this.pswa = e.target.value}
                            fullWidth
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Ripeti password"
                            type="password"
                            onChange={(e) => this.pswb = e.target.value}
                            fullWidth
                        />
                        <FormGroup row>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.state.root}
                                        onChange={(e) => this.setState({root: e.target.checked})}
                                    />
                                }
                                label="Root"
                            />
                        </FormGroup>
                        <Typography variant={"h6"} color={"red"}>{this.state.errormsg}</Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.closeDialog()} color="primary">
                            Annulla
                        </Button>
                        <Button onClick={() => this.newUser()} color="primary">
                            Crea
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(style)(UserManagement)

register_plugin(require("./UserManagement").default, "UserManagement", {}, []);