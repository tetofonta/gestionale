import React from 'react';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';

import NetworkComponent from "../NetworkComponent";
import register_plugin from "../pluginHelper";

const style = theme => ({});

class MainPage extends NetworkComponent {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div>
                <Typography variant={"h2"}>HomePage for public</Typography>
                todo: <br/>
                <ul>
                    <li>Public plugin display with tiles</li>
                    <li>advertisements</li>
                </ul>
            </div>
        )
    }
}

export default withStyles(style)(MainPage)

register_plugin(require("./MainPage").default, "MainPage", {}, []);