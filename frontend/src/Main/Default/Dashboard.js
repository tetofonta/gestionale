import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';

import NetworkComponent from "../NetworkComponent";
import register_plugin from "../pluginHelper";

const style = theme => ({
    root: {
        height: '100%',
    },
    image: {
        backgroundImage: 'url(/logo.svg)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    spaced: {
        margin: theme.spacing(3, 3, 2),
    }
});

class Dashboard extends NetworkComponent {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Grid container component="main" className={this.props.classes.root}>
                <CssBaseline/>
                <Grid item xs={false} sm={4} md={7} className={this.props.classes.image}/>
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Typography className={this.props.classes.spaced} variant={"h3"}>Login effettuato con successo
                        - {this.props.navData.username}!</Typography>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(style)(Dashboard)

register_plugin(require("./Dashboard").default, "Dashboard", {}, []);