import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';

import NetworkComponent from "../NetworkComponent";
import register_plugin from "../pluginHelper"

const style = theme => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(/logo.svg)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }
});

class Login extends NetworkComponent {

    constructor(props) {
        super(props);

        this.username = "";
        this.password = "";

        this.state = {
            tiers: []
        };
    }

    login() {
        this.POST("/login",
            {
                username: this.username,
                password: this.password
            },
            {}, false)
            .then((res) => {
                if (res.code !== 200) {
                    this.setState({error: true, errormsg: res.data.msg});
                } else if (res.data.status) {
                    window.token = res.data.data.token;
                    global.userData = res.data.data.data;

                    //load plugins
                    res.data.data.data.functions.filter(e => e.state === "HIDDEN").forEach(e => this.props.navData.loadPlugin(e.path, () => console.log("loading hidden plugin", e.desc)));

                    this.props.navigate("Dashboard", userData);
                }
            })
    }

    render() {
        return (
            <Grid container component="main" className={this.props.classes.root}>
                <CssBaseline/>
                <Grid item xs={false} sm={4} md={7} className={this.props.classes.image}/>
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <div className={this.props.classes.paper}>
                        <Avatar className={this.props.classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Login nell'area privata
                        </Typography>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            label="Username"
                            autoFocus
                            onChange={(e) => this.username = e.target.value}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            autoComplete="current-password"
                            onChange={(e) => this.password = e.target.value}
                            onKeyPress={(e) => {
                                if (e.key === 'Enter')
                                    this.login()
                            }}
                        />
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={this.props.classes.submit}
                            onClick={() => this.login()}
                        >
                            Login
                        </Button>
                        {this.state.error &&
                        <Typography component="h1" variant="h6" color="secondary">
                            {this.state.errormsg}
                        </Typography>
                        }
                    </div>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(style)(Login)

register_plugin(require("./Login").default, "Login", {}, []);