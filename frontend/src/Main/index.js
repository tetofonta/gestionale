// require("babel-polyfill");
import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import * as UI from "@material-ui/core";
import CreateSvgIcon from "@material-ui/icons/utils/createSvgIcon";
import Navigator from "./Navigator";
import NetworkComponent from "./NetworkComponent"
import register_plugin from "./pluginHelper";
import IconsDescriptor from "./icons"

function createIconElement(descriptor) {
    if (descriptor.element === "Fragment") descriptor.element = React.Fragment;
    return React.createElement(descriptor.element, descriptor.args, ...descriptor.childs.map(e => createIconElement(e)))
}

global.React = React;
global.ReactDOM = ReactDOM;
global.UI = UI;
let icone = IconsDescriptor;
Object.keys(icone).forEach(e => {
    icone[e] = CreateSvgIcon(createIconElement(icone[e]), e)
});
global.Icons = icone;

global.NetworkComponent = NetworkComponent;
global.register_plugin = register_plugin;
ReactDOM.render(<Navigator/>, document.getElementById('root'));
serviceWorker.unregister();
