export default function (plugin, name, icon, appbar) {

    const object = {
        load: plugin,
        appbar: appbar,
        icon: icon,
        description: name
    };

    if (!global.plugins) global.plugins = {[name]: object};
    else global.plugins[name] = object;

    // Object.freeze(plugins[name]);
}