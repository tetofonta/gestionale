import NetworkComponent from "./NetworkComponent";
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";

import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import LogoutIcon from '@material-ui/icons/Lock';
import LoginIcon from '@material-ui/icons/VerifiedUser';
import UserIcon from '@material-ui/icons/AccessibilityNew';
import ListItemText from "@material-ui/core/ListItemText";
import MainPage from "./Default/MainPage";
import {Hidden} from "@material-ui/core";

const drawerWidth = 200;

const style = theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24,
        color: "#FFF"// keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
        color: "white"
    },
    navbar: {
        color: "white"
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
        paddingTop: theme.spacing(8),
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
});

function clsx(...classes) {
    return classes.filter(e => !!e).join(" ");
}

class Navigator extends NetworkComponent {

    constructor(props) {
        super(props);

        // window.navigate = (page) => this.navigate(page);

        window.onpopstate = (e) => {
            if (this.history.length === 0) history.back();
            let from = this.history.pop();
            this.setPage(from.name, from.data);
        };

        this.history = [];
        this.CFG = {};

        this.state = {
            name: "MainPage",
            component: require("./Default/MainPage").default,
            navData: null,
            buttons: [],
            drw_open: false
        };

        global.parent_update = () => this.forceUpdate();
        global.toolbar_update = (cb) => {
            this.setState({
                buttons: plugins[this.state.name].appbar || []
            });
        }
    }

    static getDerivedStateFromError(error) {
        return {hasError: true};
    }

    navigate(page, data = null) {
        this.history.push({
            name: this.state.name,
            data: this.state.navData
        });
        this.setPage(page, data);
    }

    setPage(page, data = null) {
        if (plugins[page] && (Object.isFrozen(plugins[page]) || true)) {
            history.pushState(null, page, page);
            this.setState({
                component: plugins[page].load,
                navData: data,
                name: page,
                buttons: plugins[page].appbar || []
            });
        } else {
            this.loadPlugin(page, () => this.setPage(page, data));
        }
    }

    loadPlugin(page, onload) {
        const script = document.createElement('script');
        script.src = `/MOD_${page}.module.min.js`;
        script.onload = () => {
            onload()
        };
        script.onerror = () => {
            console.log("nope")
        };
        document.getElementById("plugins").appendChild(script)
    }

    componentDidMount() {
        this.GET("/config.json", {}, false).then(res => {
            this.CFG = res.data;
            this.forceUpdate();
        })
    }

    render() {
        const drawerContent = <div>
            <div className={this.props.classes.toolbarIcon}>
                <IconButton onClick={() => this.setState({drw_open: false})}>
                    <ChevronLeftIcon/>
                </IconButton>
            </div>
            <Divider/>
            <List>

                {window.token && userData.functions.filter(e => !!e.path && e.state !== "HIDDEN").map(e =>
                    <ListItem button key={e.desc} onClick={() => this.navigate(e.path)}>
                        <ListItemIcon>
                            {React.createElement(Icons[e.icon])}
                        </ListItemIcon>
                        <ListItemText primary={e.desc}/>
                    </ListItem>
                )}

            </List>
            <Divider/>

            <List>
                {!window.token && <ListItem button
                                            onClick={() => this.navigate("Login", {loadPlugin: (page, onload) => this.loadPlugin(page, onload)})}>
                    <ListItemIcon>
                        <LoginIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Login"/>
                </ListItem>}
                {window.token && [
                    <ListItem button key={1} onClick={() => {
                        window.token = undefined;
                        this.navigate("Login", {loadPlugin: (page, onload) => this.loadPlugin(page, onload)})
                    }}>
                        <ListItemIcon>
                            <LogoutIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Logout"/>
                    </ListItem>,
                    <ListItem button key={2} onClick={() => {
                        this.navigate("UserManagement")
                    }}>
                        <ListItemIcon>
                            <UserIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Private Space"/>
                    </ListItem>
                ]}
            </List>
        </div>;

        return <div className={this.props.classes.root}>
            <CssBaseline/>
            <AppBar position="fixed"
                    className={clsx(this.props.classes.appBar, this.state.drw_open && this.props.classes.appBarShift)}>
                <Toolbar className={this.props.classes.toolbar}>
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="Open drawer"
                        onClick={() => {
                            this.setState({drw_open: true})
                        }}
                        className={clsx(this.props.classes.menuButton, this.state.drw_open && this.props.classes.menuButtonHidden)}
                    >
                        <MenuIcon/>
                    </IconButton>

                    <Typography className={this.props.classes.title}
                                color="textPrimary">{this.CFG.evt_name}</Typography>

                    {this.state.buttons.map(e => e)}

                </Toolbar>
            </AppBar>

            <Hidden smDown>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: clsx(this.props.classes.drawerPaper, !this.state.drw_open && this.props.classes.drawerPaperClose),
                    }}
                    open={this.state.drw_open}
                >
                    {drawerContent}
                </Drawer>
            </Hidden>

            <Hidden mdUp>
                <Drawer
                    variant="persistent"
                    classes={{
                        paper: clsx(this.props.classes.drawerPaper, !this.state.drw_open && this.props.classes.drawerPaperClose),
                    }}
                    open={this.state.drw_open}
                >
                    {drawerContent}
                </Drawer>
            </Hidden>


            <main className={this.props.classes.content}>
                {this.state.component !== null && React.createElement(this.state.component, {
                    navigate: (page, data = null) => this.navigate(page, data),
                    navData: this.state.navData,
                    CFG: this.CFG
                }, null)}
            </main>
        </div>
    }

}

export default withStyles(style)(Navigator);