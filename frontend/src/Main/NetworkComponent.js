import React from "react"

export default class NetworkComponent extends React.Component {

    constructor(props) {
        super(props);

        if (window.token && window.userData)
            this.auth = window.token
    }

    async GET(url, headers = {}, useAuth = true, json = true) {
        return await this.REQUEST("GET", url, undefined, headers, useAuth, json);
    }

    async POST(url, body, headers = {}, useAuth = true, json = true) {
        return await this.REQUEST("POST", url, body, headers, useAuth, json);
    }

    async REQUEST(verb, url, body, headers = {}, useAuth = true, json = true) {

        let head = headers;
        if (body) {
            head['Accept'] = 'application/json';
            head['Content-Type'] = 'application/json';
        }

        if (useAuth && window.token) head['Authentication'] = this.auth;

        let res = await fetch(url, {method: verb, headers: head, body: JSON.stringify(body)});
        return {
            code: res.status,
            data: json ? await res.json() : await res.text(),
            res
        };
    }

}