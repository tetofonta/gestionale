const {Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions, Button, FormControlLabel, Checkbox} = UI;

/**
 {
    request: (verb, url, body, headers = {}, useAuth = true) => this.REQUEST(verb, url, body, headers, useAuth),
    register_action: (label, evt) => {
        this.state.plugins.push({label, evt});
        this.forceUpdate()
    },
    set_totale: (eur, cent) => {
        this.totale = [eur, cent];
        this.setState({totale: Cassa.priceToText(Cassa.normalizePrice(eur, cent))})
    },
    get_totale: () => this.totale,
    priceToText: (price) => Cassa.priceToText(price),
    normalizePrice: (eur, cent) => Cassa.normalizePrice(eur, cent),
    update: () => this.forceUpdate()
    }
 */

class Plugin {

    constructor(env) {

    }

    init(env) {

    }

    product(env, product, index, whole) {
        return product;
    }

    recepit(env, cart) {
        return {}
    }

    order(env, cart) {
        return {};
    }

    reset(env) {

    }

    inject(env, key) {
        return <div key={key}/>
    }

    inject_toolbar(env) {
        return []
    }

}

global.Cassa["Plugin"] = new Plugin();