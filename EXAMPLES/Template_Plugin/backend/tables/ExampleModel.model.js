const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        id: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true
        },
        type: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: "INVALID"
        }
    };
};

module.exports.registered = function (table) {
};

module.exports.default_data = function () {
    return [
        {
            id: "15defa22",
            type: "FULL"
        }
    ]
}
;