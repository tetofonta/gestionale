const gulp = require("gulp");
const babel = require("gulp-babel");
const ModClean = require("modclean/lib/modclean");
const fs = require("fs");
const path = require("path");
let generate;
try{generate = require("./_create_config_file");} catch (_){}
const browserify = require('browserify');
const webpack = require("webpack");
const nodeExternals = require("webpack-node-externals");
const each = require('gulp-each');
const {spawn} = require('child_process');
const tar = require("tar");

const deleteFolderRecursive = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            const curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

function web_boundle(out, src) {
    console.log(out, src);
    return new Promise((resolve, reject) => {
        let bundleFs = fs.createWriteStream(out);
        let b = browserify();
        b.plugin("tinyify");
        b.add(src);
        b.bundle().pipe(bundleFs);
        bundleFs.on('finish', function () {
            resolve();
        });
    })
}

gulp.task('copy-backend', () => {
    return gulp
        .src([path.join(__dirname, './backend/src/**/*.json')])
        .pipe(each((cnt, file, done) => {
            let c = JSON.parse(cnt);
            done(null, JSON.stringify(c))
        }))
        .pipe(gulp.dest(path.join(__dirname, 'out/server')));
});
gulp.task('copy-backend-deps', (done) => {
    let stream = gulp
        .src([path.join(__dirname, './backend/node_modules/**/*')])
        .pipe(gulp.dest(path.join(__dirname, 'out/server/node_modules/')));

    stream.on('end', () => {
        let options = {
            cwd: 'out/server',
            modulesDir: 'node_modules',
            patterns: ['default:safe'],
            additionalPatterns: [],
            ignorePatterns: [],
            noDirs: false,
            dotFiles: true,
            errorHalt: true,
            removeEmptyDirs: true,
            ignoreCase: true,
            test: false,
            followSymlink: true,
            skipModules: false
        };

        let modclean = new ModClean(options);

        modclean.clean()
            .then(() => {
                done()
            });
        done();
    })
});
gulp.task('build-backend', () => {
    return gulp.src(path.join(__dirname, "./backend/src/**/*.js"))
        .pipe(babel({presets: ['es2015', 'minify']}))
        .pipe(gulp.dest(path.join(__dirname, 'out/server')))
});
gulp.task('make-conf', (done) => {
    generate(__dirname, "out/server", "out/server");
    done();
});
gulp.task('backend', gulp.series("copy-backend-deps", "build-backend", "copy-backend", "make-conf"));

gulp.task('copy-frontend', () => {
    return gulp.src(path.join(__dirname, "./frontend/public/*"))
        .pipe(gulp.dest(path.join(__dirname, "./out/server/public")))
});
gulp.task('build-frontend', (done) => {
    let stream = gulp.src(path.join(__dirname, "./frontend/src/**/*"))
        .pipe(babel({presets: ['react', 'es2015']}))
        .pipe(babel({presets: ['minify']}))
        .pipe(gulp.dest(path.join(__dirname, 'out/obj/server')));

    stream.on('end', () => {
        try {
            fs.symlinkSync(path.join(__dirname, "./frontend/node_modules"), path.join(__dirname, "out/obj/server/node_modules"));
        } catch (e) {
        }
        done()
    })
});
gulp.task('make-frontend', (done) => {
    let bundleFs = fs.createWriteStream(__dirname + '/out/server/public/index.min.js');
    let b = browserify();
    [path.join(__dirname, "./out/obj/server/Main/index.js"),
        path.join(__dirname, "./out/obj/server/Main/Default/Login.js"),
        path.join(__dirname, "./out/obj/server/Main/Default/MainPage.js"),
        path.join(__dirname, "./out/obj/server/Main/Default/Dashboard.js"),
        path.join(__dirname, "./out/obj/server/Main/Default/UserManagement.js")].forEach(e => b.add(e));
    b.bundle().pipe(bundleFs);
    bundleFs.on('finish', function () {
        console.log('finished writing the browserify file');
        done();
    });
});
gulp.task('clean-frontend', (done) => {
    deleteFolderRecursive(path.join(__dirname, "out/obj"));
    done();
});
gulp.task('frontend', gulp.series("copy-frontend", "build-frontend", "make-frontend", "clean-frontend"));

gulp.task('default', gulp.series("backend", "frontend"));

gulp.task('select', (done) => {
    selpath = "";
    if (process.argv[process.argv.length - 2] !== "-m") throw Error("usage: gulp < target > -m < DIR >");
    let selected_path = process.argv[process.argv.length - 1] || "";

    if (selected_path.startsWith("/"))
        selpath = selected_path;
    else
        selpath = path.join(process.cwd(), selected_path);

    done();
});

gulp.task('prepare', (done) => {
    // noinspection JSUndefinedPropertyAssignment,JSUndefinedPropertyAssignment
    global.module = {};
    if (selpath.endsWith("/")) selpath = selpath.substr(0, selpath.length - 1);
    global.module.name = selpath.substr(selpath.lastIndexOf("/") > -1 ? selpath.lastIndexOf("/") + 1 : 0);
    let dirs = fs.readdirSync(selpath);
    global.module.hasBackend = dirs.includes("backend");
    global.module.hasFrontend = dirs.includes("frontend");
    global.module.hasTables = global.module.hasBackend ? fs.readdirSync(path.join(selpath, "backend")).includes("tables") : false;
    global.module.hasFrontendPlugin = global.module.hasFrontend ? fs.readdirSync(path.join(selpath, "frontend")).includes(`${global.module.name}.js`) : false;
    global.module.additionalFrontend = global.module.hasFrontend ? fs.readdirSync(path.join(selpath, "frontend")).filter(e => e.endsWith("additional.js")) : [];
    console.log(global.module);

    const child = spawn('npm', ['install'], {cwd: selpath});
    child.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });
    child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
        done();
    });
});
gulp.task('build-module-backend', (done) => {
    if (!global.module.hasBackend) {
        done();
        return;
    }
    let stream = gulp.src(`${selpath}/backend/**/*.js`)
        .pipe(babel({
            presets: ['es2015', ['minify', {
                'builtIns': false
            }]]
        }))
        .pipe(gulp.dest(`${selpath}/obj`));

    stream.on('end', () => {
        webpack({
            target: "node",
            mode: "production",
            entry: `${selpath}/obj/main.js`,
            output: {
                path: `${selpath}/out/${global.module.name}`,
                filename: `main.js`,
                library: global.module.name,
                libraryTarget: 'umd',
                umdNamedDefine: true
            },
            externals: [
                "../configs",
                "../load_module",
                "../../configs",
                "../../load_module"
            ],
        }, (err, stats) => {
            if (err || stats.hasErrors()) {
                done(err || stats.compilation.errors)
            } else {
                done();
            }
        });
    })
});
gulp.task('copy-module-backend', (done) => {
    if (!global.module.hasBackend) {
        done();
        return;
    }
    gulp.src([`${selpath}/obj/tables/*.model.js`])
        .pipe(gulp.dest(`${selpath}/out/_database/models`));
    return gulp.src([`${selpath}/backend/*.json`])
        .pipe(gulp.dest(`${selpath}/out/${global.module.name}`));
});
gulp.task('build-module-frontend', (done) => {
    if (!global.module.hasFrontend) {
        done();
        return;
    }

    gulp.src(`${selpath}/public/*`)
        .pipe(gulp.dest(`${selpath}/out/public`));

    let stream = gulp.src(`${selpath}/frontend/*.js`)
        .pipe(babel({presets: ['react', 'es2015']}))
        .pipe(gulp.dest(`${selpath}/obj/frontend`));

    stream.on('end', () => {
        if (!fs.existsSync(`${selpath}/out`)) fs.mkdirSync(`${selpath}/out`);
        if (!fs.existsSync(`${selpath}/out/public`)) fs.mkdirSync(`${selpath}/out/public`);
        Promise.all([
            new Promise((resolve) => {
                if (global.module.hasFrontendPlugin) web_boundle(`${selpath}/out/public/MOD_${global.module.name}.module.min.js`, `${selpath}/obj/frontend/${global.module.name}.js`).then(() => resolve());
                else resolve()
            }),
            Promise.all(
                global.module.additionalFrontend
                    .filter(e => e.endsWith(".cassa.module.additional.js"))
                    .map(e => web_boundle(`${selpath}/out/public/${e.substr(0, e.length - 14)}.min.js`, `${selpath}/obj/frontend/${e}`))
            )
        ]).then(() => done())
    })
});
gulp.task('module-pack', (done) => {
    tar.c({
        gzip: true,
        file: `${selpath}/${global.module.name}.tar.gz`,
        cwd: `${selpath}/out`
    }, fs.readdirSync(`${selpath}/out`)).then(() => done());
});
gulp.task('module-clean', (done) => {
    deleteFolderRecursive(`${selpath}/out`);
    deleteFolderRecursive(`${selpath}/obj`);
    done();
});
gulp.task('module', gulp.series("select", "prepare", "build-module-backend", "copy-module-backend", "build-module-frontend", "module-pack", "module-clean"));

gulp.task('install-module', (done) => {
    tar.x({
        file: `${selpath}`,
        cwd: path.join(__dirname, "out/server")
    }).then(() => done())
});
gulp.task('gen-config', (done) => {
    generate(__dirname, "out/server", "out/server");
    done();
});
gulp.task('install', gulp.series('select', 'install-module', 'gen-config'));
