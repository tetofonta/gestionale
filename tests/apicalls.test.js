// noinspection JSUndefinedPropertyAssignment
global.logger_init_ok = true;
const {run} = require("../out/server/runner");
const fetch = require("node-fetch");

function sleep(ms) {
    return new Promise((res, rej) => {
        setTimeout(res, ms)
    })
}

module.exports = {
    setUp: async function (done) {
        run(65533).catch(e => {
            throw new Error(`failed to init the server (${e.message})`)
        });
        console.log("Waiting server to start (timeout 20reqs)");
        let i;
        for (i = 0; i < 20; i++) {
            try {
                let alive = await fetch("http://127.0.0.1:65533/alive");
                break;
            } catch (_) {
                console.error("Still waiting");
            }
            await sleep(1000);
        }
        if (i === 20) {
            console.error("Server unable to start.");
            throw new Error()
        }
        console.log("Server has started");
        done();
    },

    http: {
        authentication: {
            login: async function (test) {
                let data = await fetch("http://127.0.0.1:65533/login", {
                    method: "POST",
                    body: `{"username":"administrator", "password":"admin"}`,
                    headers: {'Content-Type': 'application/json'}
                });
                console.log(await data.json());
                test.done();
            }
        }
    }
};