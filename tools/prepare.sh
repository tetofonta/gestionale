CWD=$PWD
if [[ $0 == /* ]]; then
  DIR="$(dirname "$0")"
else
  DIR="$(dirname "$PWD/$0")"
fi

cd $DIR/../backend || exit
npm install
cd $DIR/../frontend || exit
npm install
cd $DIR/../ || exit
npm install
cd $CWD || exit
