CWD=$PWD
if [[ $0 == /* ]]; then
  DIR="$(dirname "$0")"
else
  DIR="$(dirname "$PWD/$0")"
fi

cd $DIR/..

for module in $(find $PWD/$1/* -maxdepth 0 -type d); do
  gulp module -m "$module"
done
