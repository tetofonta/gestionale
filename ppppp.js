const fs = require("fs");

let out = {};

let _createSvgIcon = {
    default: (desc, name) => out[name] = desc
};

let _react = {
    default: {
        createElement: (element, args, ...childs) => ({element, args, childs}),
        Fragment: "Fragment"
    }
};

fs.readdirSync("./frontend/node_modules/@material-ui/icons").filter(e => e.endsWith(".js")).forEach(e => {
    if (e === "index.js") return;
    let icon = fs.readFileSync("./frontend/node_modules/@material-ui/icons/" + e, {encoding: "utf-8"});
    icon = icon.substr(icon.indexOf("_default"), icon.indexOf("\nexports.default = _default") - icon.indexOf("_default") - 1);
    eval(icon);
});

fs.writeFileSync("./frontend/src/Main/icons.js", "export default " + JSON.stringify(out, null, 2));