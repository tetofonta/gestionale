import {run} from "./runner"

let debug = typeof v8debug === 'object';
console.log(`Running (PID ${process.pid}) - ${process.platform} ${debug ? "DEBUG MODE" : ""}`);

run().catch(e => {
    console.error(e)
});