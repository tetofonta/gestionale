import path from "path"

export default function (module_name) {
    const object = require(path.join(__dirname, module_name, "module.dat.json"));

    if (object.library) {
        const module = require(path.join(__dirname, module_name, object.iface)).default;
        Object.freeze(module);
        return module
    }

    throw Error("Trying to call a non api module")
}