const Sequelize = require("sequelize");
const crypto = require("crypto");

module.exports.register = function () {
    return {
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
            unique: "username_u"
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        salt: {
            type: Sequelize.STRING,
            allowNull: true
        },
        enabled: {
            type: Sequelize.BOOLEAN,
            allowNull: false
        },
        isRoot: {
            type: Sequelize.BOOLEAN,
            allowNull: false
        }
    };
};

module.exports.registered = function (table) {
    console.log("Registring creation hook");
    table.beforeCreate((user, options) => {
        let salt = Math.random().toString(36).substr(2);
        let shasum = crypto.createHash('sha1');
        shasum.update(user.password + salt);
        user.salt = salt;
        user.enabled = true;
        user.password = shasum.digest('hex');
    });
    console.log("Registring update hook");
    table.beforeUpdate((user, options) => {
        if (user.changed().indexOf("password") > -1) {
            let salt = Math.random().toString(36).substr(2);
            let shasum = crypto.createHash('sha1');
            shasum.update(user.password + salt);
            user.salt = salt;
            user.password = shasum.digest('hex');
        }
    });
};

module.exports.default_data = function () {
    return [
        {
            username: "administrator",
            password: "aa8ccbb918178297f89f63392ca71333b1fef086",
            salt: "3r5war47mtk",
            enabled: 1,
            isRoot: 1
        }
    ]
};