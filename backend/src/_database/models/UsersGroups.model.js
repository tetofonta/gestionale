const Sequelize = require("sequelize");
const crypto = require("crypto");

module.exports.register = function () {
    return {
        username: {
            type: Sequelize.STRING,
            primaryKey: true,
            allowNull: false,
            references: {
                model: 'Users',
                key: 'username'
            }
        },
        group: {
            type: Sequelize.STRING,
            primaryKey: true,
            allowNull: false
        },
    };
};

module.exports.registered = function (table) {
};

module.exports.default_data = function () {
    return [
        {
            username: "administrator",
            group: "Amministrazione"
        }
    ]
};