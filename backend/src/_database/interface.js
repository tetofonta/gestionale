import Sequelize from "sequelize"
import cfg from "../configs"

const CFG = cfg("_database");

async function connect() {

    if (CFG.dialect === "sqlite")
        global.sequelize = new Sequelize({dialect: "sqlite", storage: CFG.storage});
    else if (CFG.use_url)
        global.sequelize = new Sequelize(process.env[CFG.env_url]);
    else
        global.sequelize = new Sequelize(CFG.db, CFG.user, process.env[CFG.env_psw], {
            host: CFG.host,
            dialect: CFG.dialect,
            pool: CFG.pool
        });

    sequelize.tables = {};
    await sequelize.authenticate();

    let tables = Object.keys(CFG.models);

    for (let table of tables) {
        const t = require(table);
        const tableName = CFG.models[table];

        const o = t.register();
        sequelize.tables[tableName] = await sequelize.define(tableName, o);

        Object.keys(o).forEach(e => {
            const o = t.register()[e];
            if (o.references) {
                sequelize.tables[o.references.model].hasMany(sequelize.tables[tableName], {foreignKey: o.references.key});
                sequelize.tables[tableName].belongsTo(sequelize.tables[o.references.model], {foreignKey: e});

                console.log(`Created relationship ${o.references.model}.${o.references.key} n<=>1 ${tableName}.${e}`)
            }
        });
        try {
            await sequelize.tables[tableName].findAll({attributes: Object.keys(o)});
            if (CFG.rebuild) { // noinspection ExceptionCaughtLocallyJS
                throw new Error();
            }
        } catch (e) {
            console.log("RECREATING " + tableName + e);
            await sequelize.tables[tableName].drop();
            await sequelize.tables[tableName].sync();

            let data = t.default_data();
            await Promise.all(data.map(async (e) => {
                await sequelize.tables[tableName].build(e).save();
            }))
        }
        t.registered(sequelize.tables[tableName]);
    }

    return sequelize;
}

function get(table) {
    return sequelize.tables[table]
}

export default {connect, get};