import cfg from "../configs"
import load from "../load_module"

import crypto from "crypto"
import jwt from "jsonwebtoken"

const CFG = cfg("_auth");
const CFG_g = cfg("global");
const db = load("_database");

const capitals_reg = "(?=.*[A-Z])";
const letters_reg = "(?=.*[a-z])";
const symbols_reg = "(?=.*[\\!$@_\\-=+:\\.></?])";
const numbers_reg = "(?=.*[0-9])";
const len_reg = (num) => `\\S{${num},}`;

let regex = new RegExp(`^${CFG.psw_must_contain_capitals ? capitals_reg : ""}${CFG.psw_must_contain_letters ? letters_reg : ""}${CFG.psw_must_contain_numbers ? numbers_reg : ""}${CFG.psw_must_contain_symbols ? symbols_reg : ""}${len_reg(CFG.psw_min_len)}$`);

export async function authenticate(username, password, plugins) {
    try {
        console.log(`Trying to authenticate user ${username}`);
        let user = await db.get("Users").findOne({
            where:
                {username},
            include:
                [{
                    model: db.get("UsersGroups"),
                    required: false
                },
                    {
                        model: db.get("UsersData"),
                        required: false
                    },]
        });
        if (user === null) {
            console.error(`Cannot authenticate ${username}: not found`);
            return `Utente inesistente`;
        }
        if (!user.enabled) {
            console.error(`Cannot authenticate ${username}: disabled`);
            return `L'utente ${username} é disabilitato`;
        }

        let shasum = crypto.createHash('sha1');
        shasum.update(password + user.salt);

        if (!(user.password === shasum.digest('hex'))) {
            console.error(`Cannot authenticate ${username}: invalid password`);
            return `Password errata`;
        }

        let userData = {
            date: Date().now,
            username: username,
            groups: user.UsersGroups.map(e => e.group),
            isRoot: user.isRoot,
            data: user.UsersData.map(e => {
                let d = e.get();
                delete d.username;
                delete d.createdAt;
                delete d.updatedAt;
                return d;
            })[0],
            enabled: true,
            _random: Math.random().toString(36).substr(-2) + Math.random().toString(36).substr(-2) + Math.random().toString(36).substr(-2) + Math.random().toString(36).substr(-2)
        };

        userData.functions = plugins.filter(e => new GroupPolicy(e.groups, e.root).verify(userData)).map(e => {
            return {icon: e.icon, desc: e.desc, path: e.exposes, state: e.state}
        });

        console.log("User has been authenticated");
        return {
            token: await jwt.sign(userData, CFG.secret),
            data: userData
        };
    } catch (e) {
        console.log(e);
        return "ciao";
    }
}

export async function get_user_list() {
    let user = await db.get("Users").findAll({
        include:
            [{
                model: db.get("UsersGroups"),
                required: false
            },
                {
                    model: db.get("UsersData"),
                    required: false
                },]
    });

    return user.map(e => {
        return {
            username: e.username,
            groups: e.UsersGroups.map(e => e.group),
            isRoot: e.isRoot,
            data: [e.UsersData[0]].map(e => {
                let d = e.get();
                delete d.username;
                delete d.createdAt;
                delete d.updatedAt;
                return d;
            })[0],
            enabled: e.enabled
        }
    });
}

export async function create_user(username, password, isroot, data) {

    if (password.match(regex) === null) return `New password does not satisfy required conditions.`;

    try {
        await db.get("Users").build({username, password, isRoot: isroot, enabled: true}).save();
        if (data) {
            data.username = username;
            await db.get("UsersData").build(data).save();
        }
    } catch (e) {
        return true;
    }

    return false;
}

export async function delete_user(username) {
    let user = await db.get("Users").destroy({where: {username}});
    if (user === null) {
        console.error(`Cannot delete ${username}: not found`);
        return `Cannot delete ${username}: not found`;
    }
    return false;
}

export async function add_user_group(username, group) {
    try {
        await db.get("UsersGroups").build({username, group}).save();
    } catch (e) {
        return true;
    }
    return false;
}

export async function remove_user_group(username, group) {
    let user = db.get("UsersGroups").destroy({
        where: {username, group}
    });
    return false;
}

export async function edit_user_data(username, modifyData, isroot = false) {
    let user = await db.get("UsersData").findOne({
        where:
            {username}
    });
    if (user === null) {
        console.error(`Cannot modify ${username}: not found`);
        return `Cannot modify ${username}: not found`;
    }

    Object.keys(modifyData).forEach((e) => {
        if (!e.startsWith("root_") || (e.startsWith("root_") && isroot)) user[e] = modifyData[e]
    });

    try {
        await user.save();
    } catch (e) {
        return "Error.";
    }

    return false;
}

export async function edit_user_enabled_flag(username, value) {
    let user = await db.get("Users").findByPk(username);
    if (user === null) {
        console.error(`Cannot modify ${username}: not found`);
        return false;
    }

    user.enabled = value;
    await user.save();

    return false;
}

export async function change_password(username, newpassword) {
    console.log(`Trying to authenticate user ${username}`);
    let user = await db.get("Users").findOne({
        where:
            {username}
    });
    if (user === null) {
        console.error(`Cannot authenticate ${username}: not found`);
        return `Impossibile cambiare la password: utente inesistente`;
    }

    if (newpassword.match(regex) === null) return `La nuova password non soddisfa i criteri di sicurezza.`;

    user.password = newpassword;
    user.save();
    return false;
}

export function with_authentication(groupPolicies, callback) {
    return async (req, res) => {
        //check the token header
        if (!req.headers.authentication)
            return res.answer(403, false, null, "NO authentication header found");

        let userData;
        try {
            userData = jwt.verify(req.headers.authentication, CFG.secret);
        } catch (e) {
            return res.answer(403, false, null, "Invalid token");
        }

        Object.freeze(userData);
        if (!groupPolicies.verify(userData)) return res.answer(403, false, null, "Unprivileged");

        return await callback(req, res, userData);
    };
}

export function from_network(network, netmask, callback, negate = false) {
    if (netmask === 0) netmask = [0, 0, 0, 0];
    return async (req, res) => {
        let ip = req.ip.split(".").map(e => parseInt(e)).reduce((ret, v, i) => ret |= (v & 255) << (3 - i) * 8, 0);
        let allowed = CFG_g.root_network.reduce((ret, v, i) => ret |= ((v & 255) << (3 - i) * 8), 0);
        let mask = CFG_g.root_netmask.reduce((ret, v, i) => ret |= ((v & 255) << (3 - i) * 8), 0);


        if ((ip & mask) === (allowed & mask) && !negate)
            return await callback(req, res);

        if ((ip & mask) !== (allowed & mask) && negate)
            return await callback(req, res);

        return res.answer(403, false, null, `Forbidden Network. ${req.ip} ${ip} ${mask} ${allowed}`)
    };
}

export class GroupPolicy {

    constructor(data = [], needsRoot = false) {
        this.needroot = needsRoot;
        this.policies = data;
        this.allowall = false;

        //Array di array: valida se soddisfa una delle combinazioni di gruppi nell'array
    }

    addPolicy(...data) {
        this.policies.push(data);
        return this;
    }

    needsRoot() {
        this.needroot = true;
        return this;
    }

    allowAll() {
        this.allowall = true;
        return this;
    }

    verify(user) {
        if (this.needroot) return user.isRoot;
        if (user.isRoot || this.allowall) return true;
        if (this.policies.length === 0) return true;
        for (let needed of this.policies)
            if (needed.map(e => user.groups.indexOf(e) > -1 ? 1 : 0).reduce((a, b) => a * b, 1) === 1) return true;
        return false;
    }
}

export default {
    with_authentication,
    from_network,
    GroupPolicy
}