import fastify from "fastify"
import fastifyStatic from "fastify-static"
import urlData from "fastify-url-data"
import fs from "fs"
import path from "path"
import {StandardRequest, StandardRequestSchema, StandardResponse} from "./interface"
import cfg from "../configs"
import Ddos from "ddos";

const CFG = cfg("_webserver");

function serialize(msg) {
    if (msg.req)
        return `(HTTP ${msg.req.httpVersion} ${msg.req.method}) ${msg.req.originalUrl} from [${msg.req.headers.host}/${msg.req.ip}] ${msg.req.headers.authentication ? "(AUTH)" : ""} ${msg.req.headers["user-agent"]}`;
    if (msg.res)
        return `ANSW (${msg.responseTime}) ${msg.res.statusCode}:${msg.res.statusMessage}`;
    return msg;
}


function FakeLogger(...args) {
    this.args = args;
}

FakeLogger.prototype.info = function (msg) {
    console.log(serialize(msg));
};
FakeLogger.prototype.error = function (msg) {
    console.error(serialize(msg));
};
FakeLogger.prototype.debug = function (msg) {
    console.log(`DEBUG: ${serialize(msg)}`);
};
FakeLogger.prototype.fatal = function (msg) {
    console.error(`!!!!FATAL!!!! ${serialize(msg)}`);
};
FakeLogger.prototype.warn = function (msg) {
    console.warn(serialize(msg));
};
FakeLogger.prototype.trace = function (msg) {
    console.log(serialize(msg));
};
FakeLogger.prototype.child = function () {
    return new FakeLogger()
};

export default class HttpServer {

    constructor() {
        let obj = {http2: CFG.http2, logger: {logger: new FakeLogger()}};
        if (CFG.https)
            obj.https = {
                key: fs.readFileSync(path.join(__dirname, CFG.https_key)),
                cert: fs.readFileSync(path.join(__dirname, CFG.https_cert))
            };
        this.app = fastify(obj);
        this.app.register(fastifyStatic, {
            root: CFG.static_path,
            prefix: "/"
        });
        this.app.register(urlData);
        this.ddos = new Ddos({burst: CFG.ddos_burst, limit: CFG.ddos_limit});
        this.app.use(this.ddos.express)
    }

    static route_handler_adapter(callback) {
        return async (req, res) => {
            return await callback(new StandardRequest(req), new StandardResponse(res))
        }
    }

    listen(run_port) {
        this.app.listen(run_port || CFG.port || process.env.port, CFG.bind, () => {
            console.log(`WORKING ON PORT ${CFG.port || process.env.port}`)
        });

        this.app.decorate('notFound', (req, res) => {
            res.code(301);
            res.header("Location", `http://${req.urlData().host}:${req.urlData().port}/`);
            res.send({msg: "Oh, crap"});
        });
        this.app.setNotFoundHandler(this.app.notFound);

        if (CFG.https) {
            const http_app = fastify({logger: true});
            http_app.register(urlData);
            const ddos_http = Ddos({burst: CFG.ddos_burst, limit: CFG.ddos_limit});
            http_app.use(this.ddos.express);

            const redirect_cb = async (req, res) => {
                res.code(301);
                res.header("Location", `https://${req.urlData().host}:${req.urlData().port - 1}${req.urlData().path}${req.urlData().query ? "?" + req.urlData().query : ""}`);
                return "";
            };
            ["GET", "POST", "PUT", "DELETE", "HEAD"].forEach(method => {
                http_app.route({
                    method,
                    url: "*",
                    handler: redirect_cb
                });
            });

            http_app.listen((run_port || CFG.port || process.env.port) + 1, '0.0.0.0', () => {
                console.log(`HTTP 301 WORKING ON PORT ${(CFG.port || process.env.port) + 1}`)
            })
        }
    }

    register_endpoint(method, path, callback, schema) {
        console.log(`Registering EP ${method} ${path}`);
        this.app.route({
            method,
            url: path,
            schema: schema ? schema.get() : new StandardRequestSchema().get(),
            handler: HttpServer.route_handler_adapter(callback)
        })
    }

}