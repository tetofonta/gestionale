export class StandardRequestSchema {
    constructor() {
        this.data = {
            querystring: {},
            body: {},
            params: {},
            headers: {}
        }
    }

    addBody() {
        this.data.body = {
            type: 'object',
            required: [],
            properties: {},
        };
        return this;
    }

    addParams() {
        this.data.params = {
            type: 'object',
            required: [],
            properties: {},
        };
        return this;
    }

    addHeaders() {
        this.data.headers = {
            type: 'object',
            required: [],
            properties: {},
        };
        return this;
    }

    addQueryStringRestriction(name, type) {
        this.data.querystring[name] = {type};
        return this;
    }

    _addRestriction(restriction, name, type, required, options){
        this.data[restriction].properties[name] = {type};
        Object.keys(options).forEach(e => this.data.body.properties[name][e] = options[e]);
        if (required) this.data.body.required.push(name);
        return this;
    }

    addBodyRestriction(name, type, required, options = {}) {
        return this._addRestriction("body", name, type, required, options);
    }

    addParamsRestriction(name, type, required, options = {}) {
        return this._addRestriction("params", name, type, required, options);

    }

    addHeadersRestriction(name, type, required, options = {}) {
        return this._addRestriction("headers", name, type, required, options);
    }

    get() {
        // console.log(this.data)
        return this.data;
    }

}

export class StandardRequest {
    constructor(req) {
        this.code = req.query;
        this.body = req.body;
        this.params = req.params;
        this.headers = req.headers;
        this.raw = req.raw;
        this.ip = req.ip;
        this.hostname = req.hostname
    }
}

export class StandardResponse {
    constructor(res) {
        this.code = (code) => res.code(code);
        this.header = (name, value) => res.header(name, value);
        this.removeHeader = (name) => res.removeHeader(name);
        this.hasHeader = (name) => res.hasHeader(name);
        this.redirect = (url) => res.redirect(url);
        this.send = (data) => res.send(data);
    }

    answer(code, status, data, msg = null) {
        this.code(code);
        return {status, msg, data}
    }
}

export default {
    StandardRequestSchema,
    StandardRequest,
    StandardResponse
}