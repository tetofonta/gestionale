import path from "path"
import fs from "fs"

export default function (module_name) {
    const object = require(path.join(__dirname, module_name, "module.dat.json"));

    if (fs.existsSync(path.join(__dirname, "conf.d")))
        return require(path.join(__dirname, "conf.d", object.author, `${module_name}.json`));
    else
        return require(path.join(__dirname, "configs.json"))[object.author][module_name];
}