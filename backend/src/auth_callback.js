import {
    add_user_group,
    authenticate,
    change_password,
    create_user,
    delete_user,
    edit_user_data,
    edit_user_enabled_flag,
    from_network,
    get_user_list,
    GroupPolicy,
    remove_user_group,
    with_authentication
} from "./_auth/interface";
import {StandardRequestSchema} from "./_webserver/interface";
import configs from "./configs";

const CFG_g = configs("global");

async function login(req, res, plugins) {
    const auth = await authenticate(req.body.username, req.body.password, plugins);

    if (typeof auth === "string")
        return res.answer(403, false, null, auth);

    return res.answer(200, true, auth);
}

async function user_list(req, res) {
    return res.answer(200, true, await get_user_list())
}

async function chpsw(req, res, udata) {
    if (!udata.isRoot && req.params.user !== udata.username) return res.answer(403, false, null, "You cant change someone else's password =(");

    let out = await change_password(req.params.user, req.body.password);
    if (out) return res.answer(400, false, null, out);

    return res.answer(200, true, null, null);
}

async function new_user(req, res) {
    const out = await create_user(req.body.username, req.body.password, req.body.root, req.body.data ? req.body.data : false);
    if (!out)
        return res.answer(200, true, null, "Created.");

    return res.answer(400, false, null, out);
}

async function del_user(req, res) {
    let out = await delete_user(req.params.user);
    if (out) return res.answer(400, false, null, out);

    return res.answer(200, true, null, null);
}

async function edit_user(req, res, udata) {
    if (!udata.isRoot && req.params.user !== udata.username) return res.answer(403, false, null, "You cant change someone else's data =(");
    let out = await edit_user_data(req.params.user, req.body, udata.isRoot);
    if (out) return res.answer(400, false, null, out);
    return res.answer(200, true, out);
}

async function user_status(req, res) {
    let out = await edit_user_enabled_flag(req.params.user, req.body.enabled);
    if (out) return res.answer(400, false, null, out);

    return res.answer(200, true, null, null);
}

async function group_del(req, res) {
    if (await remove_user_group(req.params.user, req.params.grp)) return res.answer(400, false, null, "Failed.");
    return res.answer(200, true, null, "ok");
}

async function group_add(req, res) {
    if (await add_user_group(req.params.user, req.params.grp)) return res.answer(400, false, null, "Failed.");
    return res.answer(200, true, null, "ok");
}

export default function register(register_endpoint, plugins) {
    register_endpoint(
        "POST",
        "/login",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            async (res, req) => await login(res, req, plugins)),
        new StandardRequestSchema().addBody()
            .addBodyRestriction("username", "string", true)
            .addBodyRestriction("password", "string", true));

    register_endpoint(
        "GET",
        "/user/list",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            with_authentication(new GroupPolicy().needsRoot(), user_list)),
        new StandardRequestSchema());

    register_endpoint(
        "POST",
        "/user/new",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            with_authentication(new GroupPolicy().needsRoot(), new_user)),
        new StandardRequestSchema().addBody()
            .addBodyRestriction("username", "string", true)
            .addBodyRestriction("password", "string", true)
            .addBodyRestriction("root", "boolean", true)
            .addBodyRestriction("data", "object", false));

    register_endpoint(
        "POST",
        "/user/:user/chpsw",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            with_authentication(new GroupPolicy().allowAll(), chpsw)),
        new StandardRequestSchema().addBody().addBodyRestriction("password", "string", true));

    register_endpoint(
        "PUT",
        "/user/:user/group/:grp",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            with_authentication(new GroupPolicy().needsRoot(), group_add))
    );
    register_endpoint("DELETE",
        "/user/:user/group/:grp",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            with_authentication(new GroupPolicy().needsRoot(), group_del))
    );

    register_endpoint(
        "POST",
        "/user/:user/data",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            with_authentication(new GroupPolicy().allowAll(), edit_user)),
        new StandardRequestSchema().addBody());

    register_endpoint(
        "DELETE",
        "/user/:user",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            with_authentication(new GroupPolicy().needsRoot(), del_user))
    );

    register_endpoint(
        "POST",
        "/user/:user/status",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0,
            with_authentication(new GroupPolicy().needsRoot(), user_status)),
        new StandardRequestSchema().addBody().addBodyRestriction("enabled", "string", true));
}