import path from "path"
import fs from "fs"

let line = 1;

function checkPath(file) {
    if (!file) return false;
    return fs.existsSync(path.dirname(file));
}

function correctPath(file) {
    if (!file) return false;
    try {
        let res = fs.mkdirSync(path.dirname(file), {recursive: true});
        return true
    } catch (e) {
        return false
    }
}

function createStream(path) {
    if (!checkPath(path))
        if (!correctPath(path)) return undefined;

    return fs.createWriteStream(path, {flags: 'a'})
}

function getCallerFile() {
    try {
        let err = new Error();
        let callerfile;
        let currentfile;

        Error.prepareStackTrace = function (err, stack) {
            return stack;
        };

        currentfile = err.stack.shift().getFileName();
        while (err.stack.length) {
            callerfile = err.stack.shift().getFileName();
            if (currentfile !== callerfile) return callerfile;
        }
    } catch (err) {
    }
    return undefined;
}

function log(message, level) {
    let msg_preamble = `[${line++}][${level}][${new Date().toISOString()}][${getCallerFile()}]: `;

    let cache = [];
    message = message.map(e => {
        if (typeof e === "object") return JSON.stringify(e, function (key, value) {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    return "CIRCULAR REF";
                }
                cache.push(value);
            }
            return value;
        }, 2);
        return String(e);
    });
    cache = null; //garbage collection


    message = message.join("\n--------------\n").replace(/\n/g, "\n" + Array(msg_preamble.length + 1).join(" "));
    return msg_preamble + message + "\n";
}

export default function (do_file_logging, log_path, verbose) {
    if (global.logger_init_ok) return;
    global.logger_init_ok = true;
    let error_stream, log_stream;
    if (do_file_logging) {
        error_stream = createStream(path.join(__dirname, log_path, "server.error.log"));
        log_stream = createStream(path.join(__dirname, log_path, "server.log"));
    }

    console.log = function (...data) {
        let llog = log(data, "LOG");
        if (verbose) process.stdout.write(llog);
        if (do_file_logging) log_stream.write(llog);
        return 0;
    };

    console.warn = function (...data) {
        let llog = log(data, "WARN");
        if (verbose) process.stdout.write(llog);
        if (do_file_logging) log_stream.write(llog);
        return 0;
    };

    console.error = function (...data) {
        let llog = log(data, "ERROR");
        process.stderr.write(llog);
        if (do_file_logging) {
            error_stream.write(llog);
            log_stream.write(llog);
        }
        return 1;
    }
}