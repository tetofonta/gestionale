import * as apis from "./plugins.json"
import path from "path"
import load from "./load_module"
import WebServer from "./_webserver/web_adapter"
import {from_network, GroupPolicy, with_authentication} from "./_auth/interface";
import auth_register from "./auth_callback";
import configs from "./configs";
import logger from "./logger"

const db = load("_database");
const CFG_g = configs("global");

logger(CFG_g.log_on_file, CFG_g.log_path, CFG_g.verbose);
let debug = typeof v8debug === 'object';

if (debug && !CFG_g.allow_dbg) {
    console.error("Debugging is not allowed here!");
    process.exit(2);
}

export async function run(run_port) {

    const ws = new WebServer();

    try {
        await db.connect();
    } catch (_) {
    }

    const plugins = await Promise.all(apis.plugins.map(async (e) => {
        const module = require(path.join(__dirname, e, "main.js"));
        Object.freeze(module);

        console.log(`Loading ${e}`);

        try {
            if (module.setup && !(await module.setup()) && module.register)
                await module.register((method, path, callback, schema) => {
                    ws.register_endpoint(method, path, callback, schema)
                });
        } catch (e) {
            console.error(e.message);
        }

        return require(path.join(__dirname, e, "module.dat.json"));
    }));


    ws.register_endpoint("GET", "/alive", async (req, res) => {
        return {"status": true, "headers": req.headers}
    });

    ws.register_endpoint("DELETE", "/quit",
        from_network(CFG_g.root_network, CFG_g.only_auth_from_op_network ? CFG_g.root_netmask : 0, with_authentication(
            new GroupPolicy().needsRoot(),
            async (req, res) => process.exit(0))));

    auth_register((method, path, callback, schema) => {
        ws.register_endpoint(method, path, callback, schema)
    }, plugins);

    ws.listen(run_port);
    global.started = true
}