const fs = require('fs');
const path = require('path');
const {exec} = require('child_process');

const useConfigDir = true; //If false a single file will be generated

function WalkModules(dir) {
    dir = path.resolve(dir);
    const list = fs.readdirSync(dir);
    let modules = [];

    list.forEach(e => {
        if (fs.statSync(path.join(dir, e)).isDirectory()) {
            let descriptor = null;
            try {
                descriptor = require(path.join(dir, e, "module.dat.json"));
                descriptor.fn = path.join(dir, e);
                descriptor.fnn = e;
                modules.push(descriptor)
            } catch (e) {
            }
        }
    });
    return modules;
}

module.exports = function (basedir, src, dest) {
    let configFile = {};
    let apis = [];
    let frontend = undefined;
    let dbexpansion = undefined;
    try {
        frontend = require("./out/server/public/config");
    } catch (_) {
    }

    WalkModules(path.join(basedir, src)).forEach(e => {
        if (!e.author) {
            console.error(`On module ${e.fn}: Cannot find author property!`);
            return;
        }

        if (!configFile[e.author]) configFile[e.author] = {};
        configFile[e.author][e.fnn] = e.configs;

        if (!e.library) apis.push(e.fnn);
    });

    let apis_final = apis;
    // while(apis.length > 0){
    //     let obj = apis.pop();
    //     if(!obj[1] && obj[1].length === 0){
    //         apis_final.push(obj[0]);
    //         continue;
    //     }
    //
    //     if(! obj[1].map(e => apis_final.indexOf(e) >= 0).reduce((a, b) => a && b, true)) apis.unshift(obj);
    //     else apis_final.push(obj[0]);
    // }

    if (!useConfigDir) {
        fs.writeFileSync(path.join(basedir, dest, "configs.json"), JSON.stringify(configFile, null, 2), {encoding: "utf-8"});
        fs.writeFileSync(path.join(basedir, dest, "plugins.json"), JSON.stringify({plugins: apis_final}, null, 2), {encoding: "utf-8"});
    } else {
        fs.mkdir(path.join(basedir, dest, "conf.d"), () => {
            Object.keys(configFile).forEach(author => {
                Object.keys(configFile[author]).forEach(module => {
                    const cfg = configFile[author][module];
                    if (cfg.frontend_cfg && frontend) {
                        Object.keys(cfg.frontend_cfg).forEach(e => frontend[e] = cfg.frontend_cfg[e]);
                        delete cfg.frontend_cfg
                    }
                    if (cfg.has_models) {
                        if (!dbexpansion) dbexpansion = {};
                        Object.keys(cfg.has_models).forEach(e => dbexpansion[e] = cfg.has_models[e]);
                        delete cfg.has_models
                    }
                    fs.mkdir(path.join(basedir, dest, "conf.d", author), () => {
                        if (!fs.existsSync(path.join(basedir, dest, "conf.d", author, `${module}.json`)))
                            fs.writeFileSync(path.join(basedir, dest, "conf.d", author, `${module}.json`), JSON.stringify(cfg, null, 2), {encoding: "utf-8"})
                    });
                })
            });
            try {
                fs.writeFileSync(path.join(basedir, dest, "public/config.json"), JSON.stringify(frontend, null, 2), {encoding: "utf-8"});
            } catch (_) {
            }

            let a = undefined;
            try {
                a = require("./out/server/conf.d/Stefano/_database");
            } catch (_) {
            }
            if (a && dbexpansion) {

                Object.keys(dbexpansion).forEach(e => a.models["./models/" + e] = dbexpansion[e]);
                fs.writeFileSync(path.join(basedir, dest, "conf.d/Stefano/_database.json"), JSON.stringify(a, null, 2), {encoding: "utf-8"});
            }
        });
        fs.writeFileSync(path.join(basedir, dest, "plugins.json"), JSON.stringify({plugins: apis_final}, null, 2), {encoding: "utf-8"});
    }
};